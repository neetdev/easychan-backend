import express, { Application, Request, Response } from 'express'
import { internet } from 'faker'
import moment, { Moment } from 'moment'

import { Board, Post, Thread } from './types'
import utils from './utils'

const easychan: Application = express()
const serverEpoch: Moment = moment.utc()
const port: number = 3000
let testBoard: Board
let testThread: Thread
//let boards: Board[]

const initBoard = async () => {
    testBoard = await utils.test.generateBoard()
    testThread = testBoard.threads[0]
}
const utcStamp = utils.utcStamp

easychan.use(express.static('static'))

easychan.use((req, res, next) => {
    console.log(utcStamp(`Handling '${req.originalUrl}' via ${req.method}`))
    res.header("Access-Control-Allow-Origin", "*")
    next()
})

easychan.get('/test', (_, res: Response) => {
    res.status(200).send(testBoard)
})

easychan.get('/test/thread', (_, res: Response) => {
    res.status(200).send(testThread)
})

easychan.post('/test/thread', (req: Request, res: Response) => {
    // Process headers
    //const headers = req.headers;
    //const cookies = req.cookies;
    // Handle post
    const postnum = testBoard.lastPost++;
    let body = req.body as Partial<Post>;
    if (!body.message) {
        res.status(400).send({ statusMessage: 'no message field'})
    } else {
        const newPost: Post = {
            message: body.message,
            attachments: body.attachments ?? [],
            title: body.title ?? "",
            username: body.username ?? "Anonymous",
            email: body.email ?? "",
            board: "test",
            timestamp: moment.utc(),
            uid: internet.color(),
            bump: !body.email?.includes("sage"),
            post: postnum,
            thread: body.thread || testThread.op.post,
            checksum: "DEADBEEF"
        }
        testBoard.threads[0].posts.concat(newPost);
    }

})

easychan.get('/test/thread/:postNum', (req: Request, res: Response) => {
    // Return the thread containing the given post
    const threadId: number = parseInt(req.params.postNum) || 0
    const payload = testBoard.threads[threadId] || {};
    const rcode = Object.keys(payload).length !== 0 ? 200 : 404
    res.status(rcode).send(payload)
})

//easychan.get('/test/board', (_, res: Response) => {
//    res.status(200).send(testBoard)
//})

//easychan.get('/test/board/:id', (req: Request, res: Response) => {
//    const boardId = req.params.id
//    const payload = boards[boardId]
//
//})

easychan.get('/', (_, res: Response) => {
    res.status(200).send('GET root')
})

easychan.post('/', (_, res: Response) => {
    res.status(403).send("don't even think about it").end()
})

easychan.use((_, res: Response) => {
    res.status(404).send("not found").end()
})

easychan.use((err: any, req: Request, res: Response, next: any) => {
    res.status(500).json(err.stack)
})

easychan.listen(port, async () => {
    console.log("I HAVE EVOLVED")
    await initBoard()
    console.log(testBoard)
    console.log("test board initialized")
    console.log(utcStamp(`server started at ${serverEpoch.toLocaleString()}`))
    console.log(utcStamp(`easychan ready : ${port}`))
})
