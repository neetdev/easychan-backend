import faker from 'faker'
import moment from 'moment'

import { Board, Post, Thread } from './types'

const utcStamp = (msg: string) => {
    return `[${moment.utc().format()}] ${msg}`;
}

const boardPostManager = (start?: number) => {
    let lastPost: number = start || 0
    return {
        newPost: async () => ++lastPost,
        getLastPost: async () => lastPost
    }
}

const generatePost = async (board: string, manager: any, op?: number) => {
    const post: number = await manager.newPost();
    console.log(`generating post in thread ${op || post}`)
    const bump = !!faker.random.number({min: 0, max: 15})
    let testPost: Post = {
        thread: op ?? post,
        title: op === undefined ? faker.lorem.words(4) : "",
        board,
        post,
        bump,
        timestamp: moment.utc(),
        uid: bump ? faker.internet.color() : "Heaven",
        username: faker.random.number({min: 0, max: 10}) === 0 ? faker.name.firstName() : "Anonymous",
        email: faker.random.number({min: 0, max: 10}) === 0 ? faker.internet.email() : "",
        message: faker.lorem.sentences(faker.random.number({min: 1, max: 10})),
        attachments: [],
        checksum: ""
    }
    return testPost;
}

const generateThread = async (board: string, manager: any) => {
    const op: Post = await generatePost(board, manager)
    const postCount = faker.random.number({ min: 0, max: 30 })
    const posts: Post[] = Array(postCount)
    for (let i = 0; i < postCount; i++) {
        posts[i] = await generatePost(board, manager, op.post)
    }
    let testThread: Thread = {
        posts,
        op
    }
    return testThread;
}

const generateBoard = async () => {
    const path = "/test/"
    const name = "/test/ - generated debug data board"
    const manager = boardPostManager()
    const threadCount = faker.random.number({ min: 0, max: 15})
    const threads: Thread[] = Array(threadCount)
    for(let i = 0; i < threadCount; i++){
        threads[i] = await generateThread(path, manager)
    }
    const lastPost = await manager.getLastPost()

    let testBoard: Board = {
        threads,
        name,
        path,
        lastPost
    }
    return testBoard;
}

const utils = {
    utcStamp,
    boardPostManager,
    test: {
        generateBoard,
        generateThread,
        generatePost,
    }
}

export default utils
