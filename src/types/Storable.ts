export type Storable = {
  uri: string
  hash?: string
}