import { Post } from './Post'

export type Thread = {
    op: Post
    posts: Post[]
}