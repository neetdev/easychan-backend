import { Moment } from 'moment'

export type Post = {
    board: string
    thread: number // post number of thread parent
    post: number // board-specific post number.
    timestamp: Moment
    title: string
    uid: string
    username: string
    email: string
    message: string
    attachments: object[]
    checksum: string
    bump: boolean
}