import { Thread } from './Thread'

export type Board = {
    name: string;
    path: string;
    threads: Thread[];
    lastPost: number;
}