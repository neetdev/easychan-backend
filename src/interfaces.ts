import { Storable } from './types'

export interface StorageController {
  store(item: Storable): boolean
  retrieve(): Storable
}